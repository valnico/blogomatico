from django.conf import settings
from django.conf.urls.defaults import url
from django.conf.urls.defaults import patterns

from . import views

urlpatterns = patterns('blogomatico.views',
    url(r'^$', views.BlogsView.as_view(), name="homepage"),
    url(r'^post/(?P<post_id>[-\w]+)$', views.PostView.as_view(), name="post"),
    url(r'^edit-post/(?P<post_id>[-\w]+)$', views.handle_edit_post, name="edit-post"),
    url(r'^blog/(?P<blog_id>[-\w]+)/(?P<blog_slug>[-\w]+)$', views.PostsView.as_view(), name="posts"),
    url(r'^blog/(?P<blog_id>[-\w]+)/(?P<blog_slug>[-\w]+)/dashboard$', views.handle_blog_dashboard, name="blog-dashboard"),
    url(r'^dashboard$', views.handle_dashboard, name="dashboard"),
    url(r'^create-post$', views.handle_blog_dashboard,  name='create-post'),
    url(r'^delete-blog$', views.delete_blog, name="delete-blog"),
    url(r'^delete-post$', views.delete_post, name="delete-post"),
)


if settings.DEBUG:
    urlpatterns += patterns('django.views.generic.simple',
        url(r'^500/$', 'direct_to_template', {'template': '500.html'}),
        url(r'^404/$', 'direct_to_template', {'template': '404.html'}),
    )
