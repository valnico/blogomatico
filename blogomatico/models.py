from google.appengine.ext import db


def create_blog_slug(blog_name):
    slug = blog_name.lower().replace(' ', '-')
    return slug


class Blog(db.Model):
    name = db.StringProperty(required=True)
    slug = db.StringProperty(required=True)


class Post(db.Model):
    title = db.StringProperty(required=True)
    content = db.TextProperty(required=True)
    blog = db.ReferenceProperty(Blog, collection_name='posts')
    
