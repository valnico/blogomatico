from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render
from django.shortcuts import redirect
from django.views.generic import TemplateView
from django.views.generic import CreateView

from google.appengine.ext import db 

from models import Post
from models import Blog
from models import create_blog_slug
from forms import PostForm
from forms import CreateBlogForm


def delete_blog(request):
    blog_id = request.POST.get('blog_id')
    if blog_id:
        blog = Blog.get_by_id(long(blog_id))
        if blog:
            db.delete(blog.posts)
            blog.delete()
        return redirect(reverse('dashboard'))
    #if we get here it's either bad request or deleting a non existing blog
    raise Http404


def delete_post(request):
    post_id = request.POST.get('post_id')
    if post_id:
        post = Post.get_by_id(long(post_id))
        if post:
            post.delete()
        return redirect(reverse('blog-dashboard', kwargs={'blog_slug': post.blog.slug, 'blog_id': post.blog.key().id()}))
    #if we get here it's either bad request or deleting a non existing post
    raise Http404


def handle_edit_post(request, post_id):
    post = Post.get_by_id(long(post_id))
    if not post:
        raise Http404
    post_form = None
    if request.method == 'POST':
        post_form = PostForm(request.POST)
        if post_form.is_valid():
            form_data = post_form.cleaned_data
            post.title=form_data['title']
            post.content=form_data['content']
            post.put()
            return redirect(reverse('post', kwargs={'post_id': post.key().id()}))
    else:
        post_form = PostForm(initial={'title': post.title, 'content': post.content, 'blog_id': post.blog.key().id()})
    return render(request, 'edit_post.html', {'form': post_form, 'post': post})


class PostsView(TemplateView):
    
    template_name = "posts.html"

    def get_context_data(self, **kwargs):
        context = super(PostsView, self).get_context_data(**kwargs)
        blog_id= str(self.kwargs['blog_id'])
        blog = Blog.get_by_id(long(blog_id))
        if not blog:
            raise Http404
        posts = Post.all().filter('blog =', blog).run()
        context.update({'posts': posts, 'blog': blog})
        return context


def create_blog(blog_name):
    new_blog = Blog(name=blog_name, slug=create_blog_slug(blog_name))
    return Blog(name=blog_name, slug=create_blog_slug(blog_name))


def handle_dashboard(request):
    if request.method == 'POST':
        create_blog_form = CreateBlogForm(request.POST)
        if create_blog_form.is_valid():
            form_data = create_blog_form.cleaned_data
            new_blog = create_blog(form_data['name'])
            new_blog.put()
            return redirect(reverse('blog-dashboard', kwargs={'blog_slug': new_blog.slug, 'blog_id': new_blog.key().id()}))
    else:
        create_blog_form = CreateBlogForm()
    blogs = Blog.all().run()
    return render(request, 'dashboard.html', {'form': create_blog_form, 'blogs': blogs})



def handle_blog_dashboard(request, blog_id, blog_slug):    
    blog = Blog.get_by_id(long(blog_id))
    if not blog:
        raise Http404
    post_form = None
    if request.method == 'POST':
        post_form = PostForm(request.POST)
        if post_form.is_valid():
            form_data = post_form.cleaned_data
            new_post = Post(title=form_data['title'], content=form_data['content'], blog=blog)
            new_post.put()
            return redirect(reverse('post', kwargs={'post_id': new_post.key().id()}))
    else:
        post_form = PostForm(initial={'blog_slug': blog.slug, 'blog_id': blog.key().id()})
    return render(request, 'blog_dashboard.html', {'form': post_form, 'blog': blog, 'posts': blog.posts})


class BlogsView(TemplateView):
    
    template_name = "blogs.html"

    def get_context_data(self, **kwargs):
        context = super(BlogsView, self).get_context_data(**kwargs)
        context.update({'blogs': Blog.all().run()})
        return context


class PostsView(TemplateView):
    
    template_name = "posts.html"

    def get_context_data(self, **kwargs):
        context = super(PostsView, self).get_context_data(**kwargs)
        blog_id= self.kwargs['blog_id']
        blog = Blog.get_by_id(long(blog_id))
        if not blog:
            raise Http404
        posts = Post.all().filter('blog =', blog).run()
        context.update({'posts': posts, 'blog': blog})
        return context


class PostView(TemplateView):
    
    template_name = "post.html"

    def get_context_data(self, **kwargs):
        context = super(PostView, self).get_context_data(**kwargs)
        post_id= str(self.kwargs['post_id'])
        post = Post.get_by_id(long(post_id))
        if not post:
            raise Http404
        context.update({'post': post})
        return context
