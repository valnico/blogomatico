import unittest

from django.test.client import Client
from django.core.urlresolvers import reverse

from google.appengine.ext import db

from ndbtestcase import AppEngineTestCase

from models import Post
from models import Blog


class Thing(db.Model):
    stuff = db.StringProperty()


class BlogomaticoTestCase(AppEngineTestCase):

    def test_thing_exists(self):
        self.thing = Thing(stuff="Hi")
        self.thing.put()
        self.assertEquals(Thing.all().count(), 1)

    def test_create_blog(self):
        client = Client()
        response = client.get(reverse('homepage'))
        self.assertEquals(list(response.context['blogs']), [])
        response = client.post(reverse('dashboard'), data={'name': 'test-name' })
        self.assertEquals(Blog.all().count(), 1)
        #I would run the tests below but they fail because of the datastore delay
        # response = client.get(reverse('homepage')) 
        #blogs = list(response.context['blogs'])
        #self.assertEquals(len(blogs), 1)
        #self.assertEquals(blogs[0].name, 'test-name')


    def test_delete_blog(self):
        self.assertEquals(Blog.all().count(), 0)
        blog = Blog(name='test name', slug="test-name")
        blog.put()
        self.assertEquals(Blog.all().count(), 1)
        client = Client()
        response = client.post(reverse('delete-blog'), data={'blog_id': blog.key().id()})
        self.assertEquals(Blog.all().count(), 0)

    def test_create_post(self):
        blog = Blog(name='test name', slug="test-name")
        blog.put()
        client = Client()
        response = client.get(reverse('posts', kwargs={'blog_id': blog.key().id(), 'blog_slug': blog.slug}))
        self.assertEquals(list(response.context['posts']), [])
        response = client.post(reverse('blog-dashboard', kwargs={'blog_id': blog.key().id(), 'blog_slug': blog.slug}),
                               data={'title': 'foo', 'content': 'bar', 'blog_id': blog.key().id()})
        self.assertEquals(Post.all().count(), 1)
    
    def test_delete_post(self):
        blog = Blog(name='test name', slug="test-name")
        blog.put()
        self.assertEquals(Post.all().count(), 0)
        post = Post(title='foo', content="bar", blog=blog)
        post.put()
        self.assertEquals(Post.all().count(), 1)
        client = Client()
        response = client.post(reverse('delete-post'), data={'post_id': post.key().id()})
        self.assertEquals(Post.all().count(), 0)

    def test_edit_post(self):
        blog = Blog(name='test name', slug="test-name")
        blog.put()
        self.assertEquals(Post.all().count(), 0)
        post = Post(title='foo', content="bar", blog=blog)
        post.put()
        self.assertEquals(Post.all().count(), 1)
        client = Client()
        response = client.post(reverse('edit-post', kwargs={'post_id': post.key().id()}), 
                               data={'title': 'new-foo', 'content': 'new-bar', 'blog_id': blog.key().id()})
        response = client.get(response.get('location'))
        modified_post = response.context['post']
        self.assertEquals(modified_post.title, 'new-foo')
        self.assertEquals(modified_post.content, 'new-bar')
        self.assertEquals(modified_post.blog.key().id(), blog.key().id())


if __name__ == '__main__':
    unittest.main()
