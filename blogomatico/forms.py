from django import forms
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError

from models import Blog
from models import create_blog_slug


class PostForm(forms.Form):
    title = forms.CharField(label='Post title', max_length=50)
    content = forms.CharField(label='Post content', max_length=10000)
    blog_id = forms.CharField(widget=forms.HiddenInput())


class CreateBlogForm(forms.Form):
    
    name = forms.CharField(label='Blog name', max_length=50, 
                           validators=[RegexValidator(regex='[\w ]+', message='This field must contain only letters or numbers', code='non_alphanumeric')])
    
    def clean_name(self):
        name = self.cleaned_data['name']
        slug = create_blog_slug(name)
        result = Blog.all().filter('slug =', slug).get()
        if result:
            raise ValidationError("Blog name already in use, choose a different one")
        return name
